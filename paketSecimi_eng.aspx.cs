﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webÖdev
{
    public partial class paketSecimi_eng : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            double toplam = 6200;
            if (TextBox1.Text != "0")
            {
                toplam += 250 * int.Parse(TextBox1.Text);
            }
            if (TextBox2.Text != "0")
            {
                toplam += 270 * int.Parse(TextBox2.Text);
                toplam += 200;
            }
            if (TextBox3.Text != "0")
            {
                toplam += 230 * int.Parse(TextBox3.Text);
                toplam += 200;
            }
            if (TextBox4.Text != "0")
            {
                toplam += 170 * int.Parse(TextBox4.Text);
                toplam += 200;
            }
            if (TextBox5.Text != "0")
            {
                toplam += 350 * int.Parse(TextBox5.Text);
                toplam += 200;
            }
            if (TextBox6.Text != "0")
            {
                toplam += 300 * int.Parse(TextBox6.Text);
                toplam += 200;
            }
            toplam = toplam / 5;
            Label8.Text = toplam.ToString();
            Label8.Text += " $";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Home/Index_eng");
        }
    }
}