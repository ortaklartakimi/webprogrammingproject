﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using webÖdev.Models;

namespace webÖdev.Controllers
{
    public class adminKullaniciController : Controller
    {
        private kullaniciEntities db = new kullaniciEntities();

        // GET: adminKullanici
        public ActionResult Index()
        {
            return View(db.tblKullanicis.ToList());
        }

        // GET: adminKullanici/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblKullanici tblKullanici = db.tblKullanicis.Find(id);
            if (tblKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblKullanici);
        }

        // GET: adminKullanici/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: adminKullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Kadi,Adi,Soyadi,Eposta,Sifre")] tblKullanici tblKullanici)
        {
            if (ModelState.IsValid)
            {
                db.tblKullanicis.Add(tblKullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblKullanici);
        }

        // GET: adminKullanici/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblKullanici tblKullanici = db.tblKullanicis.Find(id);
            if (tblKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblKullanici);
        }

        // POST: adminKullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Kadi,Adi,Soyadi,Eposta,Sifre")] tblKullanici tblKullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblKullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblKullanici);
        }

        // GET: adminKullanici/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblKullanici tblKullanici = db.tblKullanicis.Find(id);
            if (tblKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblKullanici);
        }

        // POST: adminKullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblKullanici tblKullanici = db.tblKullanicis.Find(id);
            db.tblKullanicis.Remove(tblKullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
