﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using webÖdev.Models;

namespace webÖdev.Controllers
{
    public class paket_secimiController : Controller
    {
        private paketSecimiEntities1 db = new paketSecimiEntities1();

        // GET: paket_secimi
        public ActionResult Index()
        {
            return View(db.paket_secimi.ToList());
        }

        // GET: paket_secimi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_secimi paket_secimi = db.paket_secimi.Find(id);
            if (paket_secimi == null)
            {
                return HttpNotFound();
            }
            return View(paket_secimi);
        }

        // GET: paket_secimi/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: paket_secimi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Rio_de_Janeiro,Brasilia,Salvador,Curitiba,Fortaleza,Sao_Paulo")] paket_secimi paket_secimi)
        {
            if (ModelState.IsValid)
            {
                db.paket_secimi.Add(paket_secimi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paket_secimi);
        }

        // GET: paket_secimi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_secimi paket_secimi = db.paket_secimi.Find(id);
            if (paket_secimi == null)
            {
                return HttpNotFound();
            }
            return View(paket_secimi);
        }

        // POST: paket_secimi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Rio_de_Janeiro,Brasilia,Salvador,Curitiba,Fortaleza,Sao_Paulo")] paket_secimi paket_secimi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paket_secimi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paket_secimi);
        }

        // GET: paket_secimi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_secimi paket_secimi = db.paket_secimi.Find(id);
            if (paket_secimi == null)
            {
                return HttpNotFound();
            }
            return View(paket_secimi);
        }

        // POST: paket_secimi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            paket_secimi paket_secimi = db.paket_secimi.Find(id);
            db.paket_secimi.Remove(paket_secimi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
