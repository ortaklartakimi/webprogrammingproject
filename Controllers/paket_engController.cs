﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using webÖdev.Models;

namespace webÖdev.Controllers
{
    public class paket_engController : Controller
    {
        private paketler_engEntities1 db = new paketler_engEntities1();

        // GET: paket_eng
        public ActionResult Index()
        {
            var paket_eng = db.paket_eng.Include(p => p.fiyatlar_eng);
            return View(paket_eng.ToList());
        }
        public ActionResult kullaniciPaket_eng()
        {
            var paket_eng = db.paket_eng.Include(p => p.fiyatlar_eng);
            return View(paket_eng.ToList());
        }

        // GET: paket_eng/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_eng paket_eng = db.paket_eng.Find(id);
            if (paket_eng == null)
            {
                return HttpNotFound();
            }
            return View(paket_eng);
        }
        public ActionResult kullaniciDetails_eng(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_eng paket_eng = db.paket_eng.Find(id);
            if (paket_eng == null)
            {
                ViewBag.title = "HAta ";
                return HttpNotFound();
            }
            return View(paket_eng);
        }

        // GET: paket_eng/Create
        public ActionResult Create()
        {
            ViewBag.mevsimID = new SelectList(db.fiyatlar_eng, "mevsimID", "expence1");
            return View();
        }

        // POST: paket_eng/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "mevsimID,day1,day2,day3,day4,day5,day6,day7,day8,day9,day10")] paket_eng paket_eng)
        {
            if (ModelState.IsValid)
            {
                db.paket_eng.Add(paket_eng);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.mevsimID = new SelectList(db.fiyatlar_eng, "mevsimID", "expence1", paket_eng.mevsimID);
            return View(paket_eng);
        }

        // GET: paket_eng/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_eng paket_eng = db.paket_eng.Find(id);
            if (paket_eng == null)
            {
                return HttpNotFound();
            }
            ViewBag.mevsimID = new SelectList(db.fiyatlar_eng, "mevsimID", "expence1", paket_eng.mevsimID);
            return View(paket_eng);
        }

        // POST: paket_eng/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "mevsimID,day1,day2,day3,day4,day5,day6,day7,day8,day9,day10")] paket_eng paket_eng)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paket_eng).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.mevsimID = new SelectList(db.fiyatlar_eng, "mevsimID", "expence1", paket_eng.mevsimID);
            return View(paket_eng);
        }

        // GET: paket_eng/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket_eng paket_eng = db.paket_eng.Find(id);
            if (paket_eng == null)
            {
                return HttpNotFound();
            }
            return View(paket_eng);
        }

        // POST: paket_eng/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            paket_eng paket_eng = db.paket_eng.Find(id);
            db.paket_eng.Remove(paket_eng);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
