﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using webÖdev.Models;

namespace webÖdev.Controllers
{
    public class paketAdminController : Controller
    {
        private paketlerEntities1 db = new paketlerEntities1();

        // GET: paketAdmin
        public ActionResult Index()
        {
            var pakets = db.pakets.Include(p => p.fiyatlar);
            return View(pakets.ToList());
        }

        // GET: paketAdmin/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket paket = db.pakets.Find(id);
            if (paket == null)
            {
                return HttpNotFound();
            }
            return View(paket);
        }

        // GET: paketAdmin/Create
        public ActionResult Create()
        {
            ViewBag.mevsimID = new SelectList(db.fiyatlars, "mevsimID", "gider1");
            return View();
        }

        // POST: paketAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "mevsimID,gun1,gun2,gun3,gun4,gun5,gun6,gun7,gun8,gun9,gun10")] paket paket)
        {
            if (ModelState.IsValid)
            {
                db.pakets.Add(paket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.mevsimID = new SelectList(db.fiyatlars, "mevsimID", "gider1", paket.mevsimID);
            return View(paket);
        }

        // GET: paketAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket paket = db.pakets.Find(id);
            if (paket == null)
            {
                return HttpNotFound();
            }
            ViewBag.mevsimID = new SelectList(db.fiyatlars, "mevsimID", "gider1", paket.mevsimID);
            return View(paket);
        }

        // POST: paketAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "mevsimID,gun1,gun2,gun3,gun4,gun5,gun6,gun7,gun8,gun9,gun10")] paket paket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.mevsimID = new SelectList(db.fiyatlars, "mevsimID", "gider1", paket.mevsimID);
            return View(paket);
        }

        // GET: paketAdmin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paket paket = db.pakets.Find(id);
            if (paket == null)
            {
                return HttpNotFound();
            }
            return View(paket);
        }

        // POST: paketAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            paket paket = db.pakets.Find(id);
            db.pakets.Remove(paket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
