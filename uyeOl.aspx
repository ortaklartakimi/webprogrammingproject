﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uyeOl.aspx.cs" Inherits="webÖdev.uyeOl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 429px;
            height: 220px;
        }
        .auto-style6 {
            width: 82px;
        }
        .auto-style4 {
            width: 5px;
        }
        .auto-style7 {
            width: 18px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Üye Ol</h1>
        </div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style6">Kullanıcı Adı</td>
                <td class="auto-style4">:</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtKadi" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">Adı</td>
                <td class="auto-style4">:</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtAdi" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">Soyadı</td>
                <td class="auto-style4">:</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtSoyadi" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">EPosta</td>
                <td class="auto-style4">:</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtEposta" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">Şifre</td>
                <td class="auto-style4">:</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtSifre" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">
                    <asp:Button ID="btnAnasayfa" runat="server" Height="28px" OnClick="btnAnasayfa_Click" Text="Anasayfa" Width="133px" />
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td class="auto-style7">
                    <asp:Button ID="btnKaydet" runat="server" Height="27px" OnClick="btnKaydet_Click" Text="Kaydet" Width="138px" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
